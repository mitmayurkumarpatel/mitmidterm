package inventory;
import java.util.List;
/**
 *
 * @author Mit Patel
 */
public class Simulation 
{  
    private String qty;
    private String name;
    private String id;
    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }
   public Simulation(String qty, String name, String id) {
        this.qty = qty;
        this.name = name;
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

   
    
    
}
