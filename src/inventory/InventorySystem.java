/*AUTHOR : MIT PATEL
STUDENT ID : 991634433
*/
package inventory;

import java.util.Scanner;

/**
 *author Mit Patel
 */
public class InventorySystem {
    public static void main(String[] args) 
    {      
        Item item1 = new Item(0, 0, null);//No ID, quantity or name assigned.
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter name of the item to add:");
        String name = input.nextLine();
        
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
        
        Item item2 = new Item(++item1.itemID, quantity, name);
        item1.addItem(item2);
        Item item3 = new Item(++item1.itemID, 5, "ET-2750 printer");
        item1.addItem(item3);

        Item item4 = new Item(++item1.itemID, 10, "MX-34 laptops");
        item1.addItem(item4);
        
        Item item5 = new Item(++item1.itemID, 10, "my laptop");        
        item1.addItem(item5);
        item1.printInventory();   
        System.out.println("Enter ID of the item whose quantity you want to find:");
        int temp_ID = input.nextInt();
        System.out.println("Item's quantity is: " + item1.getItemQuantity(temp_ID));

    }

}
