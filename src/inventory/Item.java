package inventory;

/*
 * @author Mit Patel
 */
public class Item 
{

     int itemID;
    private String name;
    private int quantity;

   
    public static Item[] inventory = new Item[100];
    private int itemCounter = 0; // counts numbers of items in the inventory.

  
    public Item(int itemID, int quantity, String name) 
    {
        this.itemID = itemID;
        this.quantity = quantity;
        this.name = name;
    }

    public int getItemID() {
        return itemID;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public static Item[] getInventory() {
        return inventory;
    }

    public int getItemCounter() {
        return itemCounter;
    }

    public void addItem(Item newItem) 
    {
        inventory[itemCounter] = newItem;
        itemCounter++;
        quantity++;
    }

    public void printInventory() 
    {
        for (int i = 0; i < itemCounter; i++) 
        {
            System.out.println("ID: " + inventory[i].itemID
                    + "\t Name: " + inventory[i].name
                    + "\t Quantity:" + inventory[i].quantity);
        }
    }

 
    public int getItemQuantity(int ID) 
    {
        int temp = 0;
        for (int j = 0; j < itemCounter; j++) 
        {
            if (inventory[j].itemID == ID) 
            {
                temp = inventory[j].quantity;
                break;
            }
        }
        return temp;
    }

}
